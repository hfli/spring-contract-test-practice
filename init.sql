drop table if exists customer;
drop table if exists vehicle;
drop table if exists customer_vehicle;

create table customer(
	customer_id char(4) not null primary key,
	name varchar(20) not null,
	gender char(1) not null,
	mobile varchar(20),
	birthday date,
	remark varchar(200)
);

create table customer_vehicle(
	customer_vehicle_id int not null primary key,
	customer_id char(4) not null,
	vehicle_id char(5) not null
);

create table vehicle(
	vehicle_id char(5) not null,
	vin varchar(20),
	license_plate_number varchar(10),
	mileage int,
	remark varchar(200)
);

insert into vehicle (vehicle_id, vin, license_plate_number, mileage, remark)
values
('00001', 'LGWEF3A578B001001', 'JA·00001', 100, ''),
('00002', 'LGWEF3A578B001002', 'JB·12345', 1200, ''),
('00003', 'LGWEF3A578B001003', 'JC·89098', 3450, ''),
('00004', 'LGWEF3A578B001004', 'JD·78827', 2345, ''),
('00005', 'LGWEF3A578B001005', 'JE·18274', 3009, '');

insert into customer (customer_id, name, gender, mobile, birthday, remark)
values
('0000', 'James Blue', 'M', '13345679999', '1980-08-15', ''),
('0001', 'Lily Red', 'F', '18566627772', '1991-09-26', ''),
('0002', 'Sandy Green', 'M', '13188776782', '1993-11-24', ''),
('0003', 'Bob Sky', 'F', '15578672233', '1987-05-16', ''),
('0004', 'Kody Kain', 'M', '18677882224', '1995-03-11', '');

insert into customer_vehicle(customer_vehicle_id, customer_id, vehicle_id)
values
(1, '0000', '00001'),
(2, '0000', '00002'),
(3, '0000', '00003'),
(4, '0001', '00004'),
(5, '0002', '00005');

