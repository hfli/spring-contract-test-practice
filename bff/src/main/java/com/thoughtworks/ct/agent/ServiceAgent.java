package com.thoughtworks.ct.agent;

import com.thoughtworks.ct.dto.CustomerDto;
import com.thoughtworks.ct.dto.VehicleDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;

@Component
public class ServiceAgent {

    private final RestTemplate restTemplate;

    private final String customerServiceUrl;

    private final String vehicleServiceUrl;

    @Autowired
    public ServiceAgent(RestTemplate restTemplate,
        @Value("${customer.service.url}") String customerServiceUrl,
        @Value("${vehicle.service.url}") String vehicleServiceUrl) {
        this.restTemplate = restTemplate;
        this.customerServiceUrl = customerServiceUrl;
        this.vehicleServiceUrl = vehicleServiceUrl;
    }

    public CustomerDto getCustomerDetail(String customerId) {
        URI uri = UriComponentsBuilder
            .fromUriString(customerServiceUrl)
            .pathSegment("customer", customerId)
            .build()
            .toUri();
        ResponseEntity<CustomerDto> response = restTemplate.getForEntity(uri, CustomerDto.class);
        return response.getBody();
    }

    public VehicleDto getVehicleDetail(String vehicleId) {
        URI uri = UriComponentsBuilder
            .fromUriString(vehicleServiceUrl)
            .pathSegment("vehicle", vehicleId)
            .build()
            .toUri();
        ResponseEntity<VehicleDto> response = restTemplate.getForEntity(uri, VehicleDto.class);
        return response.getBody();
    }
}
