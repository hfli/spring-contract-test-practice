package com.thoughtworks.ct.controller;

import com.thoughtworks.ct.dto.CustomerDetailDto;
import com.thoughtworks.ct.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BffController {

    @Autowired
    private CustomerService customerService;

    @GetMapping("/customer/{id}")
    CustomerDetailDto getCustomerDetail(@PathVariable String id) {
        CustomerDetailDto item = this.customerService.getCustomerDetail(id);
        return item;
    }

}
