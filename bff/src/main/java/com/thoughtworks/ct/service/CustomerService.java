package com.thoughtworks.ct.service;

import com.thoughtworks.ct.dto.CustomerDetailDto;
import com.thoughtworks.ct.agent.ServiceAgent;
import com.thoughtworks.ct.dto.CustomerDto;
import com.thoughtworks.ct.dto.VehicleDto;
import com.thoughtworks.ct.exception.RemoteInvokeException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;

import java.util.ArrayList;
import java.util.List;

@Service
public class CustomerService {

    @Autowired
    private ServiceAgent serviceAgent;

    public CustomerDetailDto getCustomerDetail(String customerId) {
        try {
            CustomerDto customer = serviceAgent.getCustomerDetail(customerId);

            List<VehicleDto> vehicleDtos = new ArrayList<>(customer.getVehicles().size());
            for (String vehicleId : customer.getVehicles()) {
                vehicleDtos.add(serviceAgent.getVehicleDetail(vehicleId));
            }
            return new CustomerDetailDto(customer, vehicleDtos);
        } catch (HttpClientErrorException ex) {
            ex.printStackTrace();
            throw new RemoteInvokeException(ex.getStatusCode(), ex.getMessage());
        }
    }
}
