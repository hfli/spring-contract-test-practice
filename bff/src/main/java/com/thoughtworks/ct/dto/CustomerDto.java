package com.thoughtworks.ct.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CustomerDto implements Serializable {

    private String customerId;

    private String name;

    private String gender;

    private String mobile;

    private Date birthday;

    private String remark;

    private List<String> vehicles = new ArrayList<>();

    public CustomerDto() {

    }

    public CustomerDto(String customerId, String name, String gender, String mobile, Date birthday, String remark,
        List<String> vehicles) {
        this.customerId = customerId;
        this.name = name;
        this.gender = gender;
        this.mobile = mobile;
        this.birthday = birthday;
        this.remark = remark;
        this.vehicles = vehicles;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public List<String> getVehicles() {
        return vehicles;
    }

    public void setVehicles(List<String> vehicles) {
        this.vehicles = vehicles;
    }
}
