package com.thoughtworks.ct.dto;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CustomerDetailDto implements Serializable {

    private String customerId;

    private String name;

    private String gender;

    private String mobile;

    private String birthday;

    private String remark;

    private List<VehicleInfo> vehicles = new ArrayList<>();

    public CustomerDetailDto() {
    }

    public CustomerDetailDto(CustomerDto customer, List<VehicleDto> vehicles) {
        this.customerId = customer.getCustomerId();
        this.name = customer.getName();
        this.gender = mapGender(customer.getGender());
        this.mobile = customer.getMobile();
        this.birthday = formatBirthday(customer.getBirthday());
        this.remark = customer.getRemark();
        this.vehicles = new ArrayList<>(vehicles.size());
        for (VehicleDto vehicleDto : vehicles) {
            this.vehicles.add(new VehicleInfo(vehicleDto));
        }
    }

    private static String mapGender(String code) {
        if ("F".equals(code)) {
            return "女";
        }
        if ("M".equals(code)) {
            return "男";
        }
        return "未知";
    }

    private static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    private static String formatBirthday(Date date) {
        if (date == null) {
            return "未知";
        }
        return dateFormat.format(date);
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public List<VehicleInfo> getVehicles() {
        return vehicles;
    }

    public void setVehicles(List<VehicleInfo> vehicles) {
        this.vehicles = vehicles;
    }

    public static class VehicleInfo {

        private String vehicleId;

        private String vin;

        private String licensePlateNumber;

        private String mileage;

        private String remark;

        public VehicleInfo(VehicleDto vehicleDto) {
            this.vehicleId = vehicleDto.vehicleId;
            this.vin = vehicleDto.vin;
            this.licensePlateNumber = vehicleDto.licensePlateNumber;
            this.mileage = vehicleDto.mileage + "公里";
            this.remark = vehicleDto.remark;
        }

        public String getVehicleId() {
            return vehicleId;
        }

        public void setVehicleId(String vehicleId) {
            this.vehicleId = vehicleId;
        }

        public String getVin() {
            return vin;
        }

        public void setVin(String vin) {
            this.vin = vin;
        }

        public String getLicensePlateNumber() {
            return licensePlateNumber;
        }

        public void setLicensePlateNumber(String licensePlateNumber) {
            this.licensePlateNumber = licensePlateNumber;
        }

        public String getMileage() {
            return mileage;
        }

        public void setMileage(String mileage) {
            this.mileage = mileage;
        }

        public String getRemark() {
            return remark;
        }

        public void setRemark(String remark) {
            this.remark = remark;
        }
    }

}
