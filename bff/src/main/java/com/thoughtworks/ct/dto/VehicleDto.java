package com.thoughtworks.ct.dto;

import java.io.Serializable;

public class VehicleDto implements Serializable {

    protected String vehicleId;

    protected String vin;

    protected String licensePlateNumber;

    protected Integer mileage;

    protected String remark;

    public VehicleDto() {
    }

    public VehicleDto(String vehicleId, String vin, String licensePlateNumber, Integer mileage, String remark) {
        this.vehicleId = vehicleId;
        this.vin = vin;
        this.licensePlateNumber = licensePlateNumber;
        this.mileage = mileage;
        this.remark = remark;
    }

    public String getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(String vehicleId) {
        this.vehicleId = vehicleId;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public String getLicensePlateNumber() {
        return licensePlateNumber;
    }

    public void setLicensePlateNumber(String licensePlateNumber) {
        this.licensePlateNumber = licensePlateNumber;
    }

    public Integer getMileage() {
        return mileage;
    }

    public void setMileage(Integer mileage) {
        this.mileage = mileage;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
