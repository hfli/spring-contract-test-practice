package com.thoughtworks.ct.advice;

import org.springframework.http.HttpStatus;

public class ErrorResponse {

    private HttpStatus error;

    private String detail;

    public ErrorResponse(HttpStatus error, String detail) {
        this.error = error;
        this.detail = detail;
    }

    public HttpStatus getError() {
        return error;
    }

    public void setError(HttpStatus error) {
        this.error = error;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }
}
