package com.thoughtworks.ct.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

public class RemoteInvokeException extends RuntimeException {

    private HttpStatus httpCode;

    public RemoteInvokeException(HttpStatus httpCode, String msg) {
        super("Failed to call service:(" + httpCode + ")" + msg);
        this.httpCode = httpCode;
    }

    public HttpStatus getHttpCode() {
        return httpCode;
    }

    public void setHttpCode(HttpStatus httpCode) {
        this.httpCode = httpCode;
    }
}
