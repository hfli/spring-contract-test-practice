package com.thoughtworks.ct.controller;

import com.thoughtworks.ct.dto.CustomerDto;
import com.thoughtworks.ct.exception.CustomerNotFoundException;
import com.thoughtworks.ct.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CustomerController {

    @Autowired
    private CustomerService customerService;

    @GetMapping("/customer/{id}")
    CustomerDto getCustomerDetail(@PathVariable String id) {
        CustomerDto item = this.customerService.getCustomer(id);
        return item;
    }

}
