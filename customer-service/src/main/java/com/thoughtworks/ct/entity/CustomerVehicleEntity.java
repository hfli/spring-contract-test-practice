package com.thoughtworks.ct.entity;

import javax.persistence.*;

import java.io.Serializable;

@Entity
@Table(name = "customer_vehicle")
public class CustomerVehicleEntity {

    @Id
    private String customerVehicleId;

    private String customerId;

    private String vehicleId;

    public CustomerVehicleEntity() {
    }

    public CustomerVehicleEntity(String customerVehicleId, String customerId, String vehicleId) {
        this.customerVehicleId = customerVehicleId;
        this.customerId = customerId;
        this.vehicleId = vehicleId;
    }

    public String getCustomerVehicleId() {
        return customerVehicleId;
    }

    public void setCustomerVehicleId(String customerVehicleId) {
        this.customerVehicleId = customerVehicleId;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(String vehicleId) {
        this.vehicleId = vehicleId;
    }
}
