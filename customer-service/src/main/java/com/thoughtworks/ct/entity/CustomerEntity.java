package com.thoughtworks.ct.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import java.util.Date;

@Entity
@Table(name = "customer")
public class CustomerEntity {

    @Id
    private String customerId;
    private String name;
    private String gender;
    private String mobile;
    private Date birthday;
    private String remark;

    public CustomerEntity() {
    }

    public CustomerEntity(String customerId, String name, String gender, String mobile, Date birthday,
        String remark) {
        this.customerId = customerId;
        this.name = name;
        this.gender = gender;
        this.mobile = mobile;
        this.birthday = birthday;
        this.remark = remark;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
