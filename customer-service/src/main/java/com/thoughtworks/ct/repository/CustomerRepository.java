package com.thoughtworks.ct.repository;

import com.thoughtworks.ct.entity.CustomerEntity;
import org.springframework.data.repository.CrudRepository;

public interface CustomerRepository extends CrudRepository<CustomerEntity, String> {

}
