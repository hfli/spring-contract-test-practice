package com.thoughtworks.ct.repository;

import com.thoughtworks.ct.entity.CustomerVehicleEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CustomerVehicleRepository extends CrudRepository<CustomerVehicleEntity, String> {

    List<CustomerVehicleEntity> findByCustomerId(String customerId);
}
