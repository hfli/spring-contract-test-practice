package com.thoughtworks.ct.service;

import com.thoughtworks.ct.dto.CustomerDto;
import com.thoughtworks.ct.entity.CustomerEntity;
import com.thoughtworks.ct.entity.CustomerVehicleEntity;
import com.thoughtworks.ct.exception.CustomerNotFoundException;
import com.thoughtworks.ct.repository.CustomerRepository;
import com.thoughtworks.ct.repository.CustomerVehicleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CustomerService {

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private CustomerVehicleRepository customerVehicleRepository;

    public CustomerDto getCustomer(String customerId) {
        CustomerEntity customer = this.customerRepository.findOne(customerId);
        if (customer == null) {
            throw new CustomerNotFoundException(customerId);
        }
        CustomerDto customerDto = CustomerDto.from(customer);
        customerDto.setVehicles(this.getCustomerVehicleIds(customerId));
        return customerDto;
    }

    public List<String> getCustomerVehicleIds(String customerId) {
        return this.customerVehicleRepository
            .findByCustomerId(customerId)
            .stream()
            .map(CustomerVehicleEntity::getVehicleId)
            .collect(Collectors.toList());
    }
}
