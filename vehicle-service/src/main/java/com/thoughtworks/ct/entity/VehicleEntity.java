package com.thoughtworks.ct.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "vehicle")
public class VehicleEntity {

    @Id
    private String vehicleId;

    private String vin;

    private String licensePlateNumber;

    private Integer mileage;

    private String remark;

    public VehicleEntity() {
    }

    public VehicleEntity(String vehicleId, String vin, String licensePlateNumber, Integer mileage, String remark) {
        this.vehicleId = vehicleId;
        this.vin = vin;
        this.licensePlateNumber = licensePlateNumber;
        this.mileage = mileage;
        this.remark = remark;
    }

    public String getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(String vehicleId) {
        this.vehicleId = vehicleId;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public String getLicensePlateNumber() {
        return licensePlateNumber;
    }

    public void setLicensePlateNumber(String licensePlateNumber) {
        this.licensePlateNumber = licensePlateNumber;
    }

    public Integer getMileage() {
        return mileage;
    }

    public void setMileage(Integer mileage) {
        this.mileage = mileage;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
