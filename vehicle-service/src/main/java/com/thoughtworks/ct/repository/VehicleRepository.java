package com.thoughtworks.ct.repository;

import com.thoughtworks.ct.entity.VehicleEntity;
import org.springframework.data.repository.CrudRepository;

public interface VehicleRepository extends CrudRepository<VehicleEntity, String> {

}
