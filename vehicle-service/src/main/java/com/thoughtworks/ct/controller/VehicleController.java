package com.thoughtworks.ct.controller;

import com.thoughtworks.ct.dto.VehicleDto;
import com.thoughtworks.ct.service.VehicleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class VehicleController {

    @Autowired
    private VehicleService vehicleService;

    @GetMapping("/vehicle/{id}")
    VehicleDto getCustomerDetail(@PathVariable String id) {
        VehicleDto item = this.vehicleService.getVehicle(id);
        return item;
    }

}
