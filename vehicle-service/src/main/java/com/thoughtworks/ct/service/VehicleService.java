package com.thoughtworks.ct.service;

import com.thoughtworks.ct.dto.VehicleDto;
import com.thoughtworks.ct.entity.VehicleEntity;
import com.thoughtworks.ct.exception.VehicleNotFoundException;
import com.thoughtworks.ct.repository.VehicleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class VehicleService {

    @Autowired
    private VehicleRepository vehicleRepository;

    public VehicleDto getVehicle(String vehicleId) {
        VehicleEntity vehicleEntity = this.vehicleRepository.findOne(vehicleId);
        if (vehicleEntity == null) {
            throw new VehicleNotFoundException(vehicleId);
        }
        return VehicleDto.from(vehicleEntity);
    }

}
